<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Language" content="ru" />

    <title>Шкафы купе в Иркутске</title>
    <meta name="title" content="Шкафы купе Иркутск" />
    <meta name="description" content="Варвара — Мебельное производство. Шкафы-купе на заказ, любых конфигураций по низким ценам в Иркутске." />
    <meta name="author" content="Варвара" />

    <link rel="shortcut icon" href="/favicon.ico" />
    <link rel="icon" href="/favicon.ico" type="image/x-icon" />

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta property="og:image" content="/img/logo.png" />
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="/css/index.css" type="text/css" />
    <link rel="stylesheet" href="/css/slick.css">
    <link rel="stylesheet" href="/css/slick-theme.css">
    <link rel="stylesheet" href="/css/magnif.css">
    <link rel="stylesheet" href="/css/animate.min.css">
    <script src="/js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
    <meta name="yandex-verification" content="2a3030c70923620d" />
    <meta name='wmail-verification' content='32b4a7acf69f5694b86e8a654b747105' />

</head>
<body>
   <div class="podarok w-100" style="display:none">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <img class="close-pod" src="/img/close-w.png" alt="">
            </div>
            <div class="firstPodarok w-100 d-flex">
                <div class="col-2 text-right align-self-center"> <img src="/img/podarok.png" class="w-75" alt=""> </div>
                <div class="col-6 align-self-center">
                    <h3>Поздравляем!</h3>
                    <p>Вы наш 5 000 посетитель сайта в этом месяце!
                    <br>Мы дарим Вам карту лояльности, с которой вы
                    <br>сможете получать скидку 3% пожизненно!</p>
                </div>
                <div class="col-4 align-self-center">
                    <button>получить подарок</button>
                </div>
            </div>
            <div class="secondPodarok w-100 d-none">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h3>Карта лояльности ваша!</h3>
                            <p>Заполните форму и менеджер уточнит детали.</p>
                        </div>
                        <form action="" class="w-100 d-flex">
                            <div class="col-4"><input type="text" hidden="hidden" name="subject" value="Запрос на карту 3% пожизненно!"><input type="text" placeholder="Имя" name="name" required></div>
                            <div class="col-4"><input type="text" name="phone" pattern=".{18,}" class="input--tel" autocomplete="off" placeholder="Телефон" required></div>
                            <div class="col-4"><button>ОТПРАВИТЬ</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
   <div class="formhid hidden-form">
       <div class="container h-100">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="col-auto">
                <div class="in">
                    <img class="close" src="/img/close-g.png" alt="">
                    <img class="logo" src="/img/logo.jpg" alt="">
                    <p><strong>ОПТИМАЛЬНЫЙ ВАРИАНТ</strong>
                    <br>НОВОГО ШКАФА КУПЕ</p>
                    <p>Мы подобрали для вас наиболее
                    <br>выгодные предложения</p>
                    <form class="form" action="" method="post">
                        <input name="subject" value="Заказ шкафа" type="text" hidden="hidden">
                        <input name="phone" pattern=".{18,}" class="input--tel" type="tel" autocomplete="off" placeholder="*введите телефон" required>
                        <button>Сделать заказ</button>
                    </form>
                    <span>я соглашаюсь с политикой конфеденциальности</span>
                </div>
            </div>
        </div>
    </div>
   </div>
   <div class="thanks hidden-thanks">
       <div class="container h-100">
            <div class="row h-100 justify-content-center align-items-center">
                <div class="col-auto">
                    <div class="in">
                        <p><big>Спасибо!</big>
                        <br>Ваша заявка принята.
                        <br>Мы свяжемся с вами
                        <br>в ближайшее время</p>
                    </div>
                </div>
            </div>
       </div>
   </div>
    <header class="header">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-4">
                    <img src="/img/logo.jpg" alt="Логотип" class="logo">
                </div>
                <div class="col-8">
                    <div class="row">
                        <div class="col-lg-6 col-12">
                            <img src="/img/address_yellow.jpg" alt="" class="icon">
                            <div class="header_align">
                                <address>г. Иркутск
                                <br>Ярославского 212А</address>
                            </div>
                        </div>
                        <div class="col-lg-6 col-12">
                            <img src="/img/phone_yellow.jpg" alt="" class="icon">
                            <div class="header_align">
                                <a href="tel:83952648240">8 (3952) 648-240</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="block-1">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center block-1__header">
                    <h1>Шкафы купе на заказ</h1>
                    <p>ЕВРОПЕЙСКОГО КАЧЕСТВА
                    <br>В ИРКУТСКЕ!</p>
                    <span>работаем по договору, где прописываем
                    <br>гарантийное обслуживание до 2 лет</span>
                </div>
                <div class="container">
                    <div class="block-1__footer">
                        <div class="row">
                            <div class="col-md-4 col-12 first">
                                <h2>Спецпредложение!</h2>
                                <p>изготовление индивидуального
                                <br>шкафа купе за <yellow>10 дней</yellow>
                                <br>от <yellow>8 499 р!</yellow> ПОД КЛЮЧ:
                                <br>
                                <br>- изготовление
                                <br>- доставка
                                <br>- установка
                                <br>- гарантия 2 года</p>
                            </div>
                            <div class="col-4 d-md-block d-none">
                                <img src="/img/block-1__item.png" alt="">
                            </div>
                            <div class="col-md-4 col-12 text-center">
                                <h3>ЗАКАЖИТЕ БЕСПЛАТНЫЙ</h3>
                                <p>замер с расчётом на дому
                                <br>прямо сейчас и не гадайте
                                <br>сколько стоит ваш шкаф</p>
                                <form class="form" action="" method="post">
                                    <input name="subject" value="Заказ замера" type="text" hidden="hidden">
                                    <input name="phone" class="input--tel" class="input--tel" type="text" autocomplete="off" pattern=".{18,}" placeholder="*ваш номер" required>
                                    <button type="submit">Заказать замер</button>
                                    <span>я согласен с политикой
                                    <br>конфеденциальности</span>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="block-2">
        <div class="container text-center">
            <div class="row">
                <div class="col-12">
                    <h2>ЛУЧШИЕ ПРЕДЛОЖЕНИЯ НА ШКАФЫ КУПЕ:</h2>
                </div>
                <div class="col-sm-3 col-12"><img src="/img/block-2__percent.png" alt="">
                <span>скидка при
                <br>оплате наличными</span></div>
                <div class="col-sm-3 col-12"><img src="/img/block-2__percent_3.png" alt=""><span>скидка
                <br>новоселам</span></div>
                <div class="col-sm-3 col-12"><img src="/img/block-2__percent.png" alt="">
                <span>скидка при
                <br>заключении
                <br>договора в день
                <br>замера</span></div>
                <div class="col-sm-3 col-12"><img src="/img/block-2__percent_year.png" alt=""><span>гарантия
                <br>на все шкафы</span></div>
            </div>
        </div>
    </div>
    <div class="block-3">
        <div class="container">
            <div class="row align-items-md-center align-items-top">
                <div class="col-12">
                    <h3 class="d-md-block d-none">Готовые шкафы купе под ключ:</h3>
                    <h5 class="d-md-none d-block text-center"><strong>Ищите оптимальный вариант?</strong>
                    <br>Мы подобрали для вас наиболее
                    <br>выгодные предложения.</h5>
                </div>
                <div class="col-xl-2 col-lg-3 col-4">
                    <img src="/img/block-3__cupe1.jpg" alt="">
                    <div class="block-3__in">
                        <p class="py-md-4 py-2"><strong>Встроенный шкаф купе</strong>
                        <br>
                        <br>- комбинированный
                        <br>- 2 двери</p>
                        <br>
                        <div class="d-block text-center">
                            <p class="text-center price-old">22600 руб.</p>
                        </div>
                    </div>
                    <button>Заказать</button>
                </div>
                <div class="col-xl-2 col-lg-3 col-4">
                    <img src="/img/block-3__cupe2.jpg" alt="">
                    <div class="block-3__in">
                        <p class="py-md-4 py-2"><strong>Корпусный шкаф купе</strong>
                        <br>
                        <br>- комбинированный
                        <br>- 2 двери</p>
                        <br>
                        <div class="d-block text-center">
                            <p class="text-center price-old">36100 руб.</p>
                        </div>
                    </div>
                    <button>Заказать</button>
                </div>
                <div class="col-xl-2 col-lg-3 col-4">
                    <img src="/img/block-3__cupe3.jpg" alt="">
                    <div class="block-3__in">
                        <p class="py-md-4 py-2"><strong>Встроенный шкаф купе</strong>
                        <br>
                        <br>- комбинированный
                        <br>- 3 двери</p>
                        <br>
                        <div class="d-block text-center">
                            <p class="text-center price-old">29700 руб.</p><br>
                        </div>
                    </div>
                    <button>Заказать</button>
                </div>
                <div class="offset-xl-1 col-xl-5 col-lg-3 d-md-block d-none col-12 block-3__before">
                    <h4><big><strong>ИЩИТЕ ОПТИМАЛЬНЫЙ ВАРИАНТ?</strong></big>
                    <br>Мы подобрали для вас наиболее
                    <br>выгодные предложения.</h4>
                    <p class="py-4">Мы являемся производителями и можем предоставить
                    <br>гибкие цены от проекта вашего шкафа. Соответственно,
                    <br>никаких промежуточных накруток не существует.</p>
                    <p><strong>Мы готовы выполнить нестандартные решения:</strong>
                    <br>Угловой шкаф купе - гардероб с линией перегиба
                    <br>Различные подсветки и тайники
                    <br>Любое количество отделений и ящиков</p>
                </div>
            </div>
        </div>
    </div>
    <div class="block-4">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="text-center">закажите сейчас, платите потом!</h2>
                </div>
                <div class="col-lg-6 offset-lg-6 col-12">
                    <h4>МОЖНО В РАССРОЧКУ НА 4 МЕСЯЦА</h4>
                    <div class="row">
                        <div class="col-lg-3 col-4">
                            <span class="block-4__bigspan">0%</span>
                            <span class="block-4__lilspan">Переплат</span>
                        </div>
                        <div class="col-lg-3 col-4">
                            <span class="block-4__bigspan">0%</span>
                            <span class="block-4__lilspan">Первоначальный
                            <br>взнос</span>
                        </div>
                        <div class="col-lg-3 col-4">
                            <span class="block-4__bigspan">0<small>РУБ</small></span>
                            <span class="block-4__lilspan">За пользование
                            <br>рассрочкой</span>
                        </div>
                    </div>
                </div>
                <div class="col-2 offset-5 d-lg-block d-none">
                    <img src="/img/cloud.png" data-wow-delay="1s" alt="" class="w-100 wow fadeInUp">
                </div>
            </div>
        </div>
        <div class="block-4__footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-12">
                        <img src="/img/block-4__girl.png" alt="">
                    </div>
                    <div class="col-lg-6 col-12 text-center block-4__footer_shmuter">
                        <p>заполните форму
                        <br><small>и получите полный расчет с оплатой
                        <br>за будущую мебель на 4 месяца</small></p>
                        <form action="" class="form" method="post">
                            <input name="subject" value="Заявка. Узнать подробнее!" type="text" hidden="hidden" required>
                            <input type="text" placeholder="Имя" name="name" required>
                            <input type="text" placeholder="Телефон" pattern=".{18,}" name="phone" class="input--tel" autocomplete="off" required>
                            <button class="wow flash">ОТПРАВИТЬ</button>
                        </form>
                        <span>я даю согласие <a href="/legal/">обработку персональных данных</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="block-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <iframe width="100%" src="https://www.youtube.com/embed/GrvdwFxliwE?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    <img src="/img/logo.jpg" alt="">
                    <h2>ШКАФЫ КУПЕ ИРКУТСК</h2>
                    <p>Шкафы купе любых конфигураций по низким ценам в Иркутске
                    <br>
                    <br>По популярности шкафы купе намного опережают любую другую корпусную мебель. В отличие от обычных шкафов, которые ограничивают функциональные и дизайнерские возможности интерьера помещения шкафы с дверями купе обеспечивают ему индивидуальность. Шкаф купе многофункционален. Он может служить в качестве гардероба, одновременно выполняя функцию разделения пространства, заменить традиционную стенку в гостиной, спрятать за зеркальной поверхностью зонтики, обувь и еще множество нужных вещей крадущих полезное пространство прихожей и эффективно организовать полезную площадь детской комнаты.
                    <br>
                    <br>Благодаря, скользящим по монорельсам раздвижным дверям шкафы купе экономят полезную площадь вашей квартиры, в отличие от распашных шкафов, которые ее загромождают. Раздвижная система двери позволяет установить встроенный шкаф в любой части вашего дома, без использования боковых стенок, пола или потолка, просто закрыв, уродующую интерьер нишу. Это очень удобная, полезная и красивая мебель с различными видами фасадов, витражами или зеркальными дверями, с широкими или узкими, открытыми или закрытыми отделениями, бесшумными раздвижными дверями и вместительным внутренним пространством для хранения вещей.
                    <br>
                    <br>На данном сайте вы можете купить шкафы купе любых размеров и конфигураций. Угловые шкафы купе с полками и большими зеркалами по индивидуальным размерам. Шкафы купе для гостиной с частично открытыми секциями, где можно поместить телевизор и музыкальный центр, а закрытые, использовать для хранения посуды, столового белья, легкой одежды, дополнительно разместив в открытой боковой консоли предметы, украшающие интерьер. По нашему каталогу вы можете купить шкаф купе подходящей к интерьеру для спальни с глубокими полками, удобными и просторными выдвижными ящиками.
                    <br>
                    <br>Покупать в наших магазинах выгодно, поскольку всегда можно найти подходящий вариант будущих шкафов для любых интерьеров. Поскольку мы не только продаем мебель, но и производим, вы можете купить шкафы, сделанные по индивидуальным заказам, либо выбрать понравившийся вам фасад, а на нашем производстве сделают реорганизацию во внутреннем пространстве шкафа купе по вашим эскизам. Таким образом, у вас будет нужное количество секций для одежды, полочек, выдвижных ящиков, металлических подставок для обуви, места для вешалок, бручниц и даже корзинки для белья.
                    <br>
                    <br>Все что нужно, это оставить нашим менеджерам заказ, оговорить сроки изготовления и выбрать материалы для оформления фасада и внутреннего наполнения. Мы обеспечим качественное и быстрое исполнение, подберем соответствующие дизайну вашей квартиры оформление, а также осуществим монтаж и все это по самым доступным ценам по Иркутску.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="block-6 d-md-block d-none mob-hide">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="text-center">Мы изготовим современный
                    <br>Шкаф купе по вашим размерам
                    <br>за 10 дней</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="block-7 d-md-block d-none mob-hide">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-12 text-center">
                    <h3>НАПОЛНЕНИЕ</h3>
                </div>
                <div class="col-auto firstimg">
                    <img src="/img/block-7__1.jpg" alt="">
                </div>
                <div class="col-auto firstimg">
                    <img src="/img/block-7__2.jpg" alt="">
                </div>
                <div class="col-auto firstimg">
                    <img src="/img/block-7__3.jpg" alt="">
                </div>
                <div class="col-auto firstimg">
                    <img src="/img/block-7__4.jpg" alt="">
                </div>
                <div class="col-auto firstimg">
                    <img src="/img/block-7__5.jpg" alt="">
                </div>
                <div class="col-auto firstimg">
                    <img src="/img/block-7__6.jpg" alt="">
                </div>
                <div class="col-auto firstimg">
                    <img src="/img/block-7__7.jpg" alt="">
                </div>
                <div class="col-auto firstimg">
                    <img src="/img/block-7__8.jpg" alt="">
                </div>
                <div class="col-auto firstimg">
                    <img src="/img/block-7__9.jpg" alt="">
                </div>
                <div class="col-12 my-5 text-center">
                    <h3>МАТЕРИАЛЫ ИСПОЛЬЗУЕМЫЕ ПРИ ПРОИЗВОДСТВЕ</h3>
                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="col-5">
                            <p class="my-2"><big>Цветное стекло</big></p>
                            <p>Крашенное стекло используется для нанесения
                            <br>на фасад двухцветного рисунка. Краски имеют
                            <br>металический блеск и невосприимчив</p>
                        </div>
                        <div class="col-7">
                            <img class="w-100" src="/img/block-7__10.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-12 my-5">
                    <div class="row">
                        <div class="col-5">
                            <p class="my-2"><big>Стекло тонированное пленками</big></p>
                            <p>Стекло, тонирование цветной пленкой,
                            <br>значительно преображается и обретает
                            <br>дизайнерскую многофункциональность</p>
                        </div>
                        <div class="col-7">
                            <img class="w-100" src="/img/block-7__11.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-12 my-5">
                    <div class="row">
                        <div class="col-6">
                            <p class="my-2"><big>Декоративные зеркала</big></p>
                            <p>С помощью данного элемента достичь эффекта
                            <br>визуального увеличения пространства в комнате</p><br>
                            <div class="row mirrors">
                                <div class="col-lg-auto">
                                    <img src="/img/mirror-1.jpg" alt="">
                                </div>
                                <div class="col-lg-auto">
                                    <img src="/img/mirror-2.jpg" alt="">
                                </div>
                                <div class="col-lg-auto">
                                    <img src="/img/mirror-3.jpg" alt="">
                                </div>
                                <div class="col-lg-auto">
                                    <img src="/img/mirror-4.jpg" alt="">
                                </div>
                                <div class="col-lg-auto">
                                    <img src="/img/mirror-5.jpg" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <p class="my-2"><big>Шелкография</big></p>
                            <p>Отличается высокой яркостью красок<br></p><br><br>
                            <div class="row windowshopper">
                                <div class="col-lg-auto">
                                    <img src="/img/shelk/1.png" alt="">
                                </div>
                                <div class="col-lg-auto">
                                    <img src="/img/shelk/2.png" alt="">
                                </div>
                                <div class="col-lg-auto">
                                    <img src="/img/shelk/3.png" alt="">
                                </div>
                                <div class="col-lg-auto">
                                    <img src="/img/shelk/4.png" alt="">
                                </div>
                                <div class="col-lg-auto">
                                    <img src="/img/shelk/5.png" alt="">
                                </div>
                                <div class="col-lg-auto">
                                    <img src="/img/shelk/6.png" alt="">
                                </div>
                                <div class="col-lg-auto">
                                    <img src="/img/shelk/7.png" alt="">
                                </div>
                                <div class="col-lg-auto">
                                    <img src="/img/shelk/8.png" alt="">
                                </div>
                                <div class="col-lg-auto">
                                    <img src="/img/shelk/9.png" alt="">
                                </div>
                                <div class="col-lg-auto">
                                    <img src="/img/shelk/10.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="row align-items-end">
                        <div class="col-6">
                            <p><big>Гравюрный рисунок</big></p>
                            <p class="my-4">Техника рисунка схожа с шелкографией, только стоит
                            <br>из сотен параллельных линий разной толщины
                            <br>Возможно нанести практически любое изображение</p>
                            <img class="w-100" src="/img/block-7__12.jpg" alt="">
                        </div>
                        <div class="offset-1 col-5">
                            <p><big>Фотопечать тонированная цветной
                            <br>пленкой</big></p>
                            <img class="w-100 mt-5" src="/img/block-7__13.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="block-8 d-md-block d-none mob-hide">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3>Варианты расположения разделителей на фасады</h3>
                    <img class="w-100" src="/img/block-8__1.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="block-9 d-md-block d-none mob-hide">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3>Фацетные элементы</h3>
                    <img class="w-100" src="/img/block-9__1.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="block-10">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2 class="d-md-block d-none mob-hide">Шкаф купе — это часть вашего интерьера,
                    <br>которая позволяет экономить
                    <br>пространство и хранить вещи</h2>
                    <h5 class="d-md-none d-block">ШКАФ КУПЕ</h5>
                </div>
                <div class="col-sm-4 col-12">
                    <img class="w-100" src="/img/block-10__1.jpg" alt="">
                    <p>В прихожую</p>
                </div>
                <div class="col-sm-4 col-12">
                    <img class="w-100" src="/img/block-10__2.jpg" alt="">
                    <p>В спальню</p>
                </div>
                <div class="col-sm-4 col-12">
                    <img class="w-100" src="/img/block-10__3.jpg" alt="">
                    <p>В гостиную</p>
                </div>
            </div>
        </div>
    </div>
    <div class="block-11">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img src="/img/logo.jpg" alt="" class="logo">
                    <div class="row">
                        <div class="col-md-4 offset-md-2 d-md-block d-none">
                            <img src="/img/block-11__item.png" class="w-100" alt="">
                        </div>
                        <div class="col-md-4 offset-md-1 col-12">
                            <h3>АКЦИЯ</h3>
                            <p>получите проект и расчет будущего
                            <br>шкафа купе бесплатно за 24 часа</p>
                            <span>заполните поля, наш мастер позвонит
                            <br>и уточнит детали</span>
                            <form action="">
                                <input name="subject" value="Заявка. Акция!" type="text" hidden="hidden" required>
                                <input type="text" placeholder="*Введите имя" name="name" required>
                                <input type="tel" placeholder="*Введите телефон" name="phone" pattern=".{18,}" class="input--tel" autocomplete="off" required>
                                <input type="text" placeholder="Ваши пожелания" name="comment">
                                <button>ОТПРАВИТЬ</button>
                                <span>я даю согласие <a href="/legal/">обработку персональных данных</a></span>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="block-12">
        <div class="container">
            <div class="row">
               <div class="col-12">
                   <h2 class="text-center">Галерея наших работ</h2>
               </div>
                <div class="col-xl-12 offset-xl-0 col-10 offset-1">
                    <div class="slick-in">
                        <img src="/img/block-12__1.jpg" href="/img/block-12__1.jpg"  alt="">
                        <img src="/img/block-12__2.jpg" href="/img/block-12__2.jpg"  alt="">
                        <img src="/img/block-12__3.jpg" href="/img/block-12__3.jpg"  alt="">
                        <img src="/img/block-12__4.jpg" href="/img/block-12__4.jpg"  alt="">
                        <img src="/img/block-12__5.jpg" href="/img/block-12__5.jpg"  alt="">
                        <img src="/img/block-12__6.jpg" href="/img/block-12__6.jpg"  alt="">
                        <img src="/img/block-12__7.jpg" href="/img/block-12__7.jpg"  alt="">
                        <img src="/img/block-12__8.jpg" href="/img/block-12__8.jpg"  alt="">
                        <img src="/img/block-12__9.jpg" href="/img/block-12__9.jpg"  alt="">
                        <img src="/img/block-12__10.jpg" href="/img/block-12__10.jpg" alt="">
                        <img src="/img/block-12__11.jpg" href="/img/block-12__11.jpg" alt="">
                        <img src="/img/block-12__12.jpg" href="/img/block-12__12.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="review">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="text-center">НАШИ ОТЗЫВЫ</h2>
                </div>
                <div class="col-sm-6 col-12">
                    <iframe width="100%" src="https://www.youtube.com/embed/DfnlFCnT4go?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
                <div class="col-sm-6 col-12">
                    <iframe width="100%" src="https://www.youtube.com/embed/CnAaS-qhHm8?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="block-13">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="text-center">БЛАГОДАРСТВЕННЫЕ ПИСЬМА</h2>
                </div>
                <div class="col-6">
                    <img class="w-100" src="/img/block-13__1.jpg" alt="">
                </div>
                <div class="col-6">
                    <img class="w-100" src="/img/block-13__2.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="block-14">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img src="/img/quot_y.jpg" alt="">
                    <p class="text-center">Мы молодая активно развивающаяся компания, 10 лет на рынке,
                    <br>свое производство, девиз нашей компании - качество по доступным ценам,
                    <br>мы постоянно совершенствуемся и всегда рады доказать клиентам что
                    <br>за хороший сервис  не обязательно переплачивать.</p>
                    <img src="/img/quot_b.jpg" class="ml-auto d-block" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="block-15">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-12">
                    <h2 class="text-center">Свяжитесь с нами
                    <br>удобным для вас способом:</h2>
                </div>
                <div class="col-md-5 offset-md-1 col-sm-8 col-12">
                    <div class="row">
                        <div class="col-12">
                            <a href="https://www.instagram.com/mebelvarvara/" target="_blank" rel="nofollow"><img src="/img/inst.png" alt=""></a>
                            <a href="https://vk.com/mebelvarvara" target="_blank" rel="nofollow"><img src="/img/vk.png" alt=""></a>
                            <a href="https://www.youtube.com/channel/UChbYTrb312MRAYxYjei4UHQ" target="_blank" rel="nofollow"><img src="/img/youtube.png" alt=""></a>
                            <a href="https://www.facebook.com/mebelvarvara38" target="_blank" rel="nofollow"><img src="/img/fb.png" alt=""></a>
                        </div>
                        <div class="col-12">
                            <a class="wow tada" data-wow-delay="2s" href="tel:83952648240"><img src="/img/phone_yellow.jpg" alt="">8 (3952) 648-240</a><br>
                            <a href="mailto:varvara@mail.ru"><img src="/img/mail_yellow.jpg" alt="">VARVARA@MAIL.RU</a><br>
                            <div><img src="/img/address_yellow.jpg" alt=""><address>Г. ИРКУТСК <br>УЛ. ЯРОСЛАВСКОГО 212А</address></div>
                            <a href="/legal/" style="font-size: .7em; margin-left: 70px">Политика конфиденциальности</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3Af4b5bb46d355de4f8e683f28f59ff9cda862a786a5577105d79e56b20d88a1ca&amp;source=constructor" width="100%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="block-16">
        <div class="container">
            <div class="row">
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <a href="http://litvinenko.digital" target="_blank"><img src="/img/logo_litvinenko.svg" alt="">— разработка сайта</a><br>
                </div>
            </div>
        </div>
    </div>
    <!-- Yandex.Metrika counter --> <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter48609785 = new Ya.Metrika({ id:48609785, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/48609785" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
    <script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
    <script type="text/javascript" src="/js/slick.min.js"></script>
    <script type="text/javascript" src="/js/jquery.magnific-popup.min.js"></script>
    <script>
        $(document).ready(function(){
            setTimeout(function() {
                $('.slick-in').slick({
                  infinite: true,
                  adaptiveHeight: true,
                  slidesToShow: 2,
                  slidesToScroll: 1,
                    responsive: [
                    {
                      breakpoint: 768,
                      settings: {
                        arrows: true,
                        slidesToShow: 1
                      }
                    }
                  ]
                });
            }, 1300);
            setTimeout(function() {
                $('.slick-in img').magnificPopup({type:'image'});
            }, 1500);
        });
    </script>
    <script src="/js/main.js"></script>
</body>
</html>
