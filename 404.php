
<!DOCTYPE html>
<html lang="ru">
<head>
    <title>404 | Варвара</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <!--    style     -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/404.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900" rel="stylesheet">
</head>
<body>
    <div class="wrapper">
        <div class="container content">
            <div class="row align-items-center">
                <div class="offset-lg-1 col-lg-6 col-12">
                    <span>404</span>
                </div>
                <div class="col-lg-5 col-12">
                    <p>ТАКОЙ СТРАНИЦЫ НЕТ</p>
                </div>
            </div>
            <div class="row">
                <div class="offset-lg-7 col-lg-4">
                    <a href="/">ВЕРНУТЬСЯ НА ГЛАВНУЮ ></a>
                </div>
            </div>
        </div>
    </div>
    <div class="footer"> <a href="https://litvinenko.digital/" target="_blank">разработка сайта - LITVINENKO</a> </div>
</body>
</html>
