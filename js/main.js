$(".block-3 button").click(function(){
    $(".hidden-form").addClass('show-form');
});
$(document).on('submit', 'form', function(e){
    var data_form = $(this).serialize();
    console.log($(this).find('input[name^="phone"]'));
    e.preventDefault();
    $.ajax({
            type: "POST", //Метод отправки
            url: "/send.php", //путь до php фаила отправителя
            data: data_form,
            success: function() {
                $('.thanks').addClass('show-thanks');
                $('.hidden-form').removeClass('show-form');
                 setTimeout(function () {
                    $('.thanks').removeClass('show-thanks');
                 }, 2000);
                $('form').trigger('reset');
                return false;
                },
            error: function() {
                $('.thanks').addClass('show-thanks');
                $('.hidden-form').removeClass('show-form');
                 setTimeout(function () {
                    $('.thanks').removeClass('show-thanks');
                 }, 2000);
                return false;
                $('form').trigger('reset');
                },
            });
});

$(document).mousedown(function (e){
    var div = $(".in");
    if (!div.is(e.target)
        && div.has(e.target).length === 0) {
        $('.hidden-form').removeClass('show-form');
    }
});
$('.close').click(function(){
    $('.hidden-form').removeClass('show-form');
});

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

//подарок
$(document).ready(function(){
    if (!navigator.cookieEnabled) {
        alert( 'Включите cookie для комфортной работы с этим сайтом' );
        return false;
    }
    if(!getCookie("visited")){
        setTimeout(function(){
     }, 12000);
    } else {
        $('.podarok').remove();
    }
            $('.podarok').fadeIn('300');
            var date = new Date(new Date().getTime() + 60 * 1000);
            date.setMonth(date.getMonth() + 1);
            document.cookie = "visited=yes; path=/; expires=" + date.toUTCString();
   if(!getCookie("visited")){
        setTimeout(function(){
     }, 12000);
    } else {
        $('.podarok').remove();
    }
});
$(".firstPodarok button").click(function(){
    $('.firstPodarok').removeClass('d-flex').addClass('d-none');
    $('.secondPodarok').removeClass('d-none').addClass('d-flex');
});
$('.close-pod').click(function(){
    if(confirm("Вы уверены? Скидка доступна Вам единожды!"))
        $('.podarok').fadeOut('slow');
});

//phone mask
$(function () {
          $('.input--tel').mask('+7 (999) 999-99-99');

          $('.input--tel').on('focus', function () {
             if ($(this).val().length === 0) {
               $(this).val('+7 (');
             }
          });
          $('.input--tel').on('focusout', function () {
              if ($(this).val().length <= 4) {
               $(this).val('');
             }
          });

          $('.input--tel').keydown(function (e) {
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                     (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                     (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                     (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                     (e.keyCode >= 35 && e.keyCode <= 39)) {
                        return;
                }

                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        });
